.. testrepo documentation master file, created by
   sphinx-quickstart on Tue Jul  2 05:33:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
Welcome to testrepo's documentation!
====================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   test

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
