# Dockerfile for testing bitbucket and getting used to debian image

FROM     debian:stretch

ENV      DEBIAN_FRONTEND=noninteractive 

RUN      apt-get update \
           && apt-get -y install python3 python3-pip python3-venv python-dev curl \
           && apt-get clean autoclean \
           && rm -rf /var/lib/{apt,dpkg,cache}/ 

RUN      mkdir -p /data \
		   && groupadd -g 1000 builduser \
		   && useradd  -u 1000 -g 1000 builduser -d /data \
           && chown builduser.builduser /data

WORKDIR  /data

ENV      USER=builduser \
         HOME=/data

USER     builduser

COPY     * /data/

RUN      python3 -m venv venv \
           && . venv/bin/activate \
		   && pip3 install -r requirements.txt \
		   && sphinx-build -b html docs html \
		   && rm -rf venv .cache/pip

WORKDIR  /data/html

RUN      python3 -m http.server 8000 & pid=$! && sleep 3 && curl http://0.0.0.0:8000/ && ( kill -KILL ${pid} || /bin/true )

CMD      python3 -m http.server 8000

